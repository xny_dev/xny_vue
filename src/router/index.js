import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Login from '../views/Login.vue'
import Index from '../views/Index.vue'
import CourseVideo from '../views/CourseVideo.vue'
import CourseArticle from '../views/CourseArticle.vue'
import Market from '../views/Market.vue'
import Supply from '../views/Supply.vue'
import Require from '../views/Require.vue'
import axios from 'axios'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/index',
    name: 'Index',
    component: Index,
    children: [
      { path: '/home', component: Home },
      { path: '/about', component: About },
      { path: '/course/video', component: CourseVideo },
      { path: '/course/article', component: CourseArticle },
      { path: '/course/market', component: Market },
      { path: '/supply', component: Supply },
      { path: '/require', component: Require },
      { path: '/about', component: About },
      { path: '/about', component: About }

    ]
  },
  {
    path: '/',
    redirect: '/index'
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  console.info(to.path)
  if (to.path === '/login') return next()
  if (!localStorage.getItem('phone')) return next('/login')
  console.info('-------->' + to)
  axios.defaults.headers.common.Authorization = sessionStorage.getItem('token')
  next()
})

export default router
